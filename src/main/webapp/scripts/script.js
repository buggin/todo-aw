$(document).ready(function() {
    $('#login-form').submit(login);
    $('#register-form').submit(register);
    $('#new-list-link').click(show_new_list_form);
    $('#new-list-cancel').click(hide_new_list_form);
    $('#error').click(hide_error);
    $('#new-list-form').submit(create_new_list);
    $('#new-todo-item-form').submit(create_new_todo_item);
    $('#one-list-section').on('change', '.todo-item', toggle_check);
    $('#sharebutton').click(shareToggle);
    $('#editbutton').click(editNameTodolist);
    $('#submit-edit-button').click(postEditTodolist);
    $('#one-list-section>a>h2').click(backtolists);
    $("#new-user-share-form").submit(insertuserpermission);
    //$('.checkbox-inline.col-md-3').onclick=();
    $('#sharing').on('click','.canAdd',sendCanAdd);
    $('#sharing').on('click','.canCheck',sendCanCheck);
    $('#sharing').on('click','.revoke-ok',sendRevoke);
    $('#sharing').on('click','.edit-ok',sendConfirmPerm);
    $('#edit').on('submit','#edit-todolist-form',editNameTodolist);
});
function sendCanAdd(uri){
    //console.log(uri.currentTarget.attributes[1].nodeValue);
    //alert("add "+uri.currentTarget.attributes[1].nodeValue);
    var addemail = uri.currentTarget.attributes[1].nodeValue;
    $.ajax({
        url : current_todo_list_uri+"/permissions",
        method : 'POST',
        success : on_revoke_success,
        error : on_error,
        data : {
            add_email : addemail
        },
    });
    return false;
}
function sendCanCheck(uri){
    alert("check "+uri.currentTarget.attributes[1].nodeValue);
    var checkemail = uri.currentTarget.attributes[1].nodeValue;
    console.log(current_todo_list_uri);
        $.ajax({
        url : current_todo_list_uri+"/permissions",
        method : 'POST',
        success : on_revoke_success,
        error : on_error,
        data : {
            check_email : checkemail
        },
    });
    return false;
}
function sendRevoke(){
    var revokeemail = this.title;
    console.log(this);
    alert("revoke "+this.title);

    $.ajax({
        url : current_todo_list_uri+"/permissions",
        method : 'POST',
        success : on_revoke_success,
        error : on_error,
        data : {
            delete_email : revokeemail
        },
    });
    return false;
}
function sendConfirmPerm(uri){
    alert("confirm");
}
function on_revoke_success(){
    shareToggle();
    backtolists();
}
function backtolists(){
    $('#one-list-section').hide(200);
    $('#sharing').hide(200);
    $('#edit').hide(200);
    $('#my-lists-section').show(200);
    $('#shared-lists-section').show(200);
    populate_my_lists();
    current_todo_list_uri="";

}
function postEditTodolist(){
    var newname = $('#new-name-todolist').val();
    //alert("che stupido nome "+ newname+" per una lista");
    $.ajax({
        url : current_todo_list_uri,
        method : 'POST',
        success : do_open_todo_list,
        error : on_error,
        data : {
            newname : newname,
        },
    });
    return false;
}
function on_error(data) {
    console.log(JSON.stringify(data, null, 2));
    var message = data.statusText;
    if (typeof data.responseJSON !== 'undefined')
        message += ': ' + data.responseJSON.message;
    $('#error').text(message).show(200);
    hide_my_lists_spinner();
}
function editNameTodolist() {
    $('#sharing').hide(200);
    if($('#edit').is(':visible'))
        $('#edit').hide(200);
    else
        $('#edit').show(200);
}
function hide_error() {
    $('#error').hide(200);
}
function shareToggle() {
    $('#edit').hide(200);
    if($('#sharing').is(':visible'))
        $('#sharing').hide(200);
    else {
        $('#sharing').show(200);
        $.ajax({
            url: current_todo_list_uri+"/users",
            method: 'GET',
            success: on_share_display,
            error: on_error,
        });
        return false;
    }
}
function on_share_display(data) {
    $('p>span>img').hide();
    MrMustache(data);
}
function MrMustache(data){
    //console.log("MUSTACHE:")
    console.log(data);
    var template = $('#sharing-template').html();
    var view = {
        users:data.users,
        canCheck:function(){
            if(this.check)
                return "checked";
            else return "";
        },
        canAdd:function(){
            if(this.add)
                return "checked";
            else return "";
        }
    }
    var rendered = Mustache.render(template, view);
    $('#peopleCanSee').html(rendered);
    //console.log("-->> allUsers:");
    console.log(data.allUsers);
    $("#share-with-text").autocomplete({
        source: data.allUsers
    });
}
function insertuserpermission(){
    console.log(data);
    var data = $('#share-with-text').val();
    var html = $('#peopleCanSee').html();
    html+= "<li class='row' ><div class='col-md-2'> <b>"+data+"</b> </div><div class='checkbox-inline col-md-3 canCheck' title='"+data+"' >    <label>    <input type='checkbox' {{&canCheck}}>Check/Uncheck</label></div><div class='checkbox-inline col-md-3 canAdd' title='"+data+"'>    <label>    <input type='checkbox' {{&canAdd}}>Add items</label></div><div class='btn-group col-md-3' role='group' aria-label='...'>    <button title='"+data+"' type='button' class='btn btn-success btn-sm edit-ok'>    <span class='glyphicon glyphicon-ok' aria-hidden='true'></span>    </button>    <button type='button' title='"+data+"' class='btn btn-danger revoke-ok btn-sm'>Revoke</button>    </div>    </li>";
    $('#peopleCanSee').html(html);
    $('#share-with-text').val("");
    sendCanRead(data)
    return false;
}
function sendCanRead(data){
    $.ajax({
        url : current_todo_list_uri+"/permissions",
        method : 'POST',
        success : on_revoke_success,
        error : on_error,
        data : {
            read_email : data
        },
    });
    return false;
}

function create_new_list() {
    show_my_lists_spinner();
    $('#new-list-link').show(200);
    $.ajax({
        url: '/todolists',
        method: 'POST',
        success: on_create_new_list_success,
        error: on_error,
        data: {
            name: $('#new-list-name').val()
        },
    });
    return false;
}

function on_create_new_list_success() {
    $('#new-list-name').val("");
    hide_new_list_form();
    populate_my_lists();
}

function show_new_list_form() {
    $('#new-list-form').show(200);
    $('#new-list-link').hide(200);
    $('#my-lists').css('margin-top',"90px");
}

function hide_new_list_form() {
    $('#new-list-form').hide(200);
    $('#new-list-link').show(200);
    $('#my-lists').css('margin-top',"10px");
}

function show_my_lists_spinner() {
    $('#my-lists-spinner').show(200);
    $('#shared-lists-spinner').show(200);
}

function hide_my_lists_spinner() {
    $('#my-lists-spinner').hide(200);
    $('#shared-lists-spinner').hide(200);
}

function populate_my_lists() {
    show_my_lists_spinner();

    $.ajax({
        url: '/todolists',
        success: on_populate_my_lists_success,
        error: on_error,
    });
    return false;
}

function on_populate_my_lists_success(data) {
    var username = data.username;
    displayName(username);
    hide_my_lists_spinner();
    var template = $('#my-lists-template').html();
    var html = Mustache.render(template, {my_lists: data.myLists});
    $('#my-lists').html(html);
    html = Mustache.render(template, {my_lists: data.sharedLists});
    $('#shared-lists').html(html);
    $('a.todolist').click(open_todo_list);
}

var current_todo_list_uri;

function open_todo_list() {
    current_todo_list_uri = $(this).attr('data-uri');
    do_open_todo_list();
}

function do_open_todo_list() {
    show_my_lists_spinner();
    $('#new-name-todolist').val("");
    $('#edit').hide(200);
    $.ajax({
        url: current_todo_list_uri,
        success: on_open_todo_list_success,
        error: on_error,
    });
    $('#new-todo-item-text').val('');
}

function on_open_todo_list_success(data) {
    hide_my_lists_spinner();
    $('#my-lists-section').hide(200);
    $('#shared-lists-section').hide(200);
    $('#one-list-section').show(200);
    $('#one-list-section h2').text(data.name);
    $('#todo-items-unchecked').html("");
    $('#todo-items-checked').html("");
    console.log(data);
    if(data.add){
        $('#new-todo-item-form').show();
    }else{
        $('#new-todo-item-form').hide();
    }
    if(data.isOwner){
        $('div.btn-group.pull-right').show();
    }else{
        $('div.btn-group.pull-right').hide();
    }
    for (var i=0; i<data.items.length; i++) {
        var text = data.items[i].text;
        var uri = data.items[i].uri;
        var is_checked = data.items[i].status == "checked";
        var checked_attr = is_checked? "checked" : "";
        var html ="";
        if(data.check) {
            html = "<li><input type='checkbox' " + checked_attr +" class='todo-item'  data-uri='" + uri + "'> " + text + "</li>";
        }
        else {
            html = "<li class='todo-item' data-uri='" + uri + "'> " + "<span class='";
            if (checked_attr)
                html+= "glyphicon glyphicon-check'> </span> " + text + "</li>";
            else
                html+="glyphicon glyphicon-unchecked'> </span> " + text + "</li>";
        }
            if (is_checked)
                $('#todo-items-checked').append(html);
            else
                $('#todo-items-unchecked').append(html);
    }

}
$(function() {
    $('#register-password').password().on('show.bs.password', function(e) {
        $('.input-addon').prop('checked', true);
    }).on('hide.bs.password', function(e) {
        $('#register-password').prop('checked', false);
    });
    $('#register-password2').password().on('show.bs.password', function(e) {
        $('.input-addon').prop('checked', true);
    }).on('hide.bs.password', function(e) {
        $('#register-password2').prop('checked', false);
    });
    $('.input-addon').click(function() {
        $('#register-password').password('toggle');
        $('#register-password2').password('toggle');
    });
});

function create_new_todo_item() {
    $.ajax({
        url: current_todo_list_uri + '/items',
        method: 'POST',
        success: do_open_todo_list,
        error: on_error,
        data: {
            text: $('#new-todo-item-text').val()
        },
    });
    return false;
}

function login() {
    var email = $('#login-email').val();
    var passw = $('#login-password').val();
    $.ajax({
        url: '/login',
        method: 'GET',
        success: on_login_success,
        error: on_error,
        data: {
            email: email,
            password: passw
        },
    });
    return false;
}
function on_login_success(){
    $('#login-alert').hide(200);
    $('#error').hide(200);
    $('#login-section').hide(200);
    $('#register-section').hide(200);
    $('#my-lists-section').show(200);
    $('#shared-lists-section').show(200);
    populate_my_lists();
}
function displayName(data){
    $('small').text("Welcome "+data+"!");
    $('small').show(200);
}
window.onbeforeunload = function() {
    return "Are you sure you want to logout?"
}
function toggle_check() {
    show_my_lists_spinner();
    var todoItem = $(this)[0];
    $.ajax({
        url: todoItem.getAttribute('data-uri'),
        method: 'POST',
        success: do_open_todo_list,
        error: on_error,
        data: {
            checked: todoItem.checked
        },
    });
    return false;
}

function register(){
    var username = $('#register-name').val();
    var email = $('#register-email').val();
    var passw = $('#register-password').val();
    var passw2 = $('#register-password2').val();
    if(username==""){
        $('#error').text("Error: Username empty!").show(200);
        $('.reg-name').addClass("has-error");
    }else{
        $('.reg-name').removeClass("has-error");
        if (email==""){
            $('#error').text("Error: Email empty!").show(200);
            $('.reg-email').addClass("has-error");
        }else{
            $('.reg-email').removeClass("has-error");
            if(passw==""|passw.length<8){
                $('#error').text("Error: Password has to be long at least 8 characters!").show(200);
                $('.register-passwords').addClass("has-error");
            }else{
                if (passw !== passw2) {
                    $('#error').text("Error: Passord unmatch!").show(200);
                    $('.register-passwords').addClass("has-error");
                } else {
                    $('.register-passwords').removeClass("has-error");
                    alert("email: " + email + "\npsw:" + passw);
                    $.ajax({
                        url: '/register',
                        method: 'GET',
                        success: on_login_success,
                        error: on_error,
                        data: {
                            name: username,
                            email: email,
                            password: passw
                        },
                    });
                    return false;
                }return false;
            }return false;
        }return false;
    }return false;
}