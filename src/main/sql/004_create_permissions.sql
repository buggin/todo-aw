CREATE TABLE permission
(
  user_id INT NOT NULL ,
  todo_list_id INT NOT NULL,
  permission_code INT NOT NULL,

  PRIMARY KEY (user_id,todo_list_id)
);