
create table users (
  id serial,
  name varchar(255) not null,
  email varchar(255) not null UNIQUE,
  psw varchar(255) not null,

  primary key(id)
);

update schema_info set version = 3;

