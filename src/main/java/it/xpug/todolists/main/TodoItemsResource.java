package it.xpug.todolists.main;

import it.xpug.toolkit.db.Permission;
import it.xpug.toolkit.db.PermissionCode;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.valueOf;
import static java.util.Collections.emptyList;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

public class TodoItemsResource extends Resource {

    private final TodoListSession session;
    private TodoListRepository todoLists;

    public TodoItemsResource(HttpServletRequest request, HttpServletResponse response, TodoListRepository todoLists, TodoListSession session) {
        super(request, response);
        this.todoLists = todoLists;
        this.session = session;
    }

    @Override
    public void service() throws IOException {

        User user = session.getUser();

        if (isPost() && uriMatches("/todolists/(\\d+)/users")) {
            if (parameterIsMissing("email")||parameterIsMissing("code")) {
                respondWith(SC_BAD_REQUEST, "Parameter 'email' and 'code' are required");
                return;
            }
            int todoListId = getUriParameterAsInt(1);
            TodoList todoList = todoLists.get(todoListId, user);
            if(todoList.getOwner().getEmail().equals(user.getEmail())){
                System.out.println("DEBUG: OWNER: code"+Integer.valueOf(request.getParameter("code")));
                User grantee = new User(request.getParameter("user"),request.getParameter("email"));
                PermissionCode code = new PermissionCode(Integer.valueOf(request.getParameter("code")));
                todoLists.grantAccess(todoList, user, grantee, new Permission(todoList,grantee,code));
                todoLists.update(todoList,user);
                response.sendRedirect("/todolists/" + todoListId);
            }
            return;
        }
        if (uriMatches("/todolists/(\\d+)/users")) {
            int todoListId = getUriParameterAsInt(1);
            TodoList todoList = todoLists.get(todoListId, user);
            JSONObject json = new JSONObject();
            json.put("users", emptyList());
            for(Sharing sharing: todoLists.getAllUsers(todoList)){
                json.append("users", new JSONObject()
                                .put("email", sharing.getUser().getEmail())
                                .put("username", sharing.getUser().getName())
                                .put("check", sharing.getPermission().canCheck())
                                .put("add",sharing.getPermission().canAdd())
                                .put("read", sharing.getPermission().canRead())
                );
            }
            for(User u: todoLists.getAllUsers()){
                json.append("allUsers", u.getEmail());
            }
            render(json);
            return;
        }
        if (uriMatches("/todolists/(\\d+)/items")) {
            if (parameterIsMissing("text")) {
                respondWith(SC_BAD_REQUEST, "Parameter 'text' is required");
                return;
            }
            int todoListId = getUriParameterAsInt(1);
            TodoList todoList = todoLists.get(todoListId,user);
            todoList.addItem(new TodoItem(request.getParameter("text")));
            todoLists.update(todoList, user);
            response.sendRedirect("/todolists/" + todoListId);
            return;
        }
        if (uriMatches("/todolists/(\\d+)/items/(\\d+)")) {
            if (parameterIsMissing("checked")) {
                respondWith(SC_BAD_REQUEST, "Parameter 'checked' is required");
                return;
            }
            int todoListId = getUriParameterAsInt(1);
            TodoList todoList = todoLists.get(todoListId,user);
            todoList.checkItem(getUriParameterAsInt(2), Boolean.valueOf(request.getParameter("checked")));
            todoLists.update(todoList,user);
            response.sendRedirect("/todolists/" + todoListId);
            return;
        }
        if (isPost() && uriMatches("/todolists/(\\d+)/permissions")) {
            if(parameterIsMissing("check_email")&&parameterIsMissing("delete_email")&&parameterIsMissing("add_email")&&parameterIsMissing("read_email")){
                respondWith(SC_BAD_REQUEST, "Parameter 'check_email' or 'delete_email' or 'add_email' or 'read_email' is required");
                return;
            }else{
//                System.out.println("Permission mode");
                String deleteEmail = request.getParameter("delete_email");
                String checkEmail = request.getParameter("check_email");
                String addEmail = request.getParameter("add_email");
                String readEmail = request.getParameter("read_email");
//                System.out.println(deleteEmail + checkEmail + addEmail);
                TodoList todoList = todoLists.get(valueOf(getUriParameter(1)), user);
                if (null!=deleteEmail){
                    User modif = todoLists.getUserInfo(deleteEmail);
                    Permission permission = todoLists.getPermission(todoList, modif);
                    permission.setRead();
                    System.out.println("CODE SET TO "+permission);
                    todoLists.grantAccess(todoList, todoList.getOwner(), modif, permission);
                }
                if (null!=checkEmail) {
                    User modif = todoLists.getUserInfo(checkEmail);
                    Permission permission = todoLists.getPermission(todoList, modif);
                    permission.setCheck();
                    todoLists.grantAccess(todoList, todoList.getOwner(), modif, permission);
                }
                if (null!=addEmail){
                    User modif = todoLists.getUserInfo(addEmail);
                    Permission permission = todoLists.getPermission(todoList, modif);
                    permission.setAdd();
                    todoLists.grantAccess(todoList, todoList.getOwner(), modif, permission);
                }
                if (null!=readEmail){
                    User modif = todoLists.getUserInfo(readEmail);
                    Permission permission = todoLists.getPermission(todoList, modif);
                    permission.setRead();
                    todoLists.grantAccess(todoList, todoList.getOwner(), modif, permission);
                }
            }
        }
    }
}
