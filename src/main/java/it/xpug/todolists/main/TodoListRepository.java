package it.xpug.todolists.main;

import it.xpug.toolkit.db.Permission;

import java.util.LinkedList;
import java.util.List;

public interface TodoListRepository {

	TodoList get(int todoListId, User user);

    int add(TodoList todoList);

    List<TodoList> getAll(User user);

	void update(TodoList todoList, User user);

	int getNItemsLeft(TodoList todoList, User user);

    void grantAccess(TodoList todoList,User owner, User anotherUser, Permission permission);

    LinkedList<Sharing> getAllUsers(TodoList todoList);

    List<User> getAllUsers();

    Permission getPermission(TodoList todolist, User user);

    void setName(TodoList todoList, String name, User user);

    User getUserInfo(String email);

    //test methods
	void clear();
	long size();

}
