package it.xpug.todolists.main;

import it.xpug.toolkit.db.Permission;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static java.lang.Integer.valueOf;
import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;

public class TodoListsResource extends Resource {

    private final TodoListSession session;
    private TodoListRepository todoLists;

    public TodoListsResource(HttpServletRequest request, HttpServletResponse response, TodoListRepository todoLists, TodoListSession session) {
        super(request, response);
        this.todoLists = todoLists;
        this.session = session;
    }

    @Override
    public void service() throws IOException {

        User user = session.getUser();

        if (isPost()
                && (parameterIsMissing("name")
                &&parameterIsMissing("newname")
        )) {
            respondWith(SC_BAD_REQUEST, "Parameter 'name' or 'newname' is required");
            return;
        }
        if (isPost()&&!(parameterIsMissing("name"))) {
            //creazione nuova todolist
            int newTodoListId = todoLists.add(new TodoList(request.getParameter("name"),user));
            response.sendRedirect("/todolists/" + newTodoListId);
            return;
        }
        if (isPost()&&uriMatches("/todolists/(\\d+)")&&!(parameterIsMissing("newname"))) {
            TodoList todoList = todoLists.get(valueOf(getUriParameter(1)), user);
            if(todoList.getOwner().getEmail().equals(user.getEmail())) {
                todoLists.setName(todoList, request.getParameter("newname"), user);
            }
        }
        if (uriMatches("/todolists/(\\d+)")) {
            //get todolist
            TodoList todoList = todoLists.get(valueOf(getUriParameter(1)), user);
            Permission permission = todoLists.getPermission(todoList,user);
//            System.out.println("DEBUG: " + permission + todoList +user);
            if(permission.canRead()) {
                respondWithTodoList(valueOf(getUriParameter(1)), user,permission);
                return;
            }
            else
                notFound();
            return;
        }

        respondWithAllTodoLists(user);
    }

    private void respondWithAllTodoLists(User user) throws IOException {
        JSONObject json = new JSONObject();
        json.put("myLists", emptyList());
        json.put("sharedLists", emptyList());
        json.put("username",user.getName());
        for (TodoList todoList : todoLists.getAll(user)) {
            Permission permission = todoLists.getPermission(todoList,user);
            if(permission.canRead())
                if(todoList.getOwner().getEmail().equals(user.getEmail())){
                    int left = todoLists.getNItemsLeft(todoList, user);
                    json.append("myLists", new JSONObject()
                                    .put("name", todoList.getName())
                                    .put("uri", format("/todolists/%d", todoList.getId()))
                                    .put("email", todoList.getOwner().getEmail())
                                    .put("check", permission.canCheck())
                                    .put("add", permission.canAdd())
                                    .put("read",permission.canRead())
                                    .put("left", left)
                    );
                }else{
                    int left = todoLists.getNItemsLeft(todoList, user);
                    json.append("sharedLists", new JSONObject()
                                    .put("name", todoList.getName())
                                    .put("uri", format("/todolists/%d", todoList.getId()))
                                    .put("owner", "from: " + todoList.getOwner().getName() + " - ")
                                    .put("permission", permission)
                                    .put("email", todoList.getOwner().getEmail())
                                    .put("check", permission.canCheck())
                                    .put("add", permission.canAdd())
                                    .put("read",permission.canRead())
                                    .put("left", left)
                    );
                }
//			System.out.println("DEBUG:Name: \""+todoList.getName()+"\" #"+todoList.getId()+" left:"+left);
        }
        render(json);
    }

    private void respondWithTodoList(Integer todoListId, User user, Permission permission) throws IOException {
        TodoList todoList = todoLists.get(todoListId, user);
        if (null == todoList) {
            notFound();
            return;
        }
        JSONObject json = todoList.toJson();
        JSONArray items = (JSONArray) json.get("items");
        for (int itemId=0; itemId<items.length(); itemId++) {
            JSONObject todoItem = (JSONObject) items.get(itemId);
            todoItem.put("uri", format("/todolists/%d/items/%d", todoListId, itemId));
        }
        json.put("code", permission.getCodeAsInt())
                .put("check", permission.canCheck())
                .put("read", permission.canRead())
                .put("isOwner", todoList.getOwner().getEmail().equals(user.getEmail()))
                .put("add",permission.canAdd());

        render(json);
    }
}
