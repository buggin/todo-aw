package it.xpug.todolists.main;

import it.xpug.toolkit.db.Permission;

/**
 * Created by john on 11/07/15.
 */
public class Sharing {
    private Permission permission;
    private User user;
    private TodoList todoList;

    public Sharing(Permission permission, User user, TodoList todoList) {
        this.permission = permission;
        this.user = user;
        this.todoList = todoList;
    }

    public Permission getPermission() {
        return permission;
    }

    public User getUser() {
        return user;
    }

    public TodoList getTodoList() {
        return todoList;
    }
}
