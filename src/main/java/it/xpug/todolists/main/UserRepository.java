package it.xpug.todolists.main;

import java.util.List;

public interface UserRepository {

    int add(String clearTextPassword, User user);

    User get(int newUserId);

    User authenticate(String email, String clearTextPassword);

    List<User> getAllUsers();

    void clear();
}
