package it.xpug.todolists.main;

import java.util.*;

public class InMemoryUserRepository implements UserRepository{

	Map<String, User> usersByPassword = new HashMap<String, User>();
    //TODO GROSSO COME UNA CASA
	public void add(User user, String clearTextPassword) {
		usersByPassword.put(clearTextPassword, user);
    }

	@Override
	public int add(String clearTextPassword, User user) {
		return 0;
	}

	@Override
	public User get(int newUserId) {
		return null;
	}

	public User authenticate(String email, String clearTextPassword) {
	    User user = usersByPassword.get(clearTextPassword);
	    if (null == user)
	    	return null;
	    if (!user.getEmail().equals(email))
	    	return null;
	    return user;
    }

    @Override
    public List<User> getAllUsers() {
        return null;
    }

    @Override
    public void clear() {
        usersByPassword = new HashMap<>();
    }

}
