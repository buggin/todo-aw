package it.xpug.todolists.main;

import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.ListOfRows;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DatabaseUserRepository implements UserRepository{

	private Database database;

	public DatabaseUserRepository(Database database) {
		this.database=database;
	}

	public int add(String clearTextPassword, User user) {
		try {
			String hash = Password.getSaltedHash(clearTextPassword);
			String sql = "insert into users (name, psw, email) values (?,?,?) returning id";
			ListOfRows rows = database.select(sql, user.getName(), hash ,user.getEmail());
			int id = (int) rows.get(0).get("id");
			return id;
		} catch (Exception e) {
			System.err.println("DB UserRepo: add(): duplicate OR Password hashing failed!");
			e.printStackTrace();
			return -1;
		}
	}

	public User get(int newUserId) {
		ListOfRows rows = database.select("select * from users where id = ?", newUserId);
		if (rows.size() == 0)
			return null;
		String name = (String) rows.get(0).get("name");
		String email = (String) rows.get(0).get("email");
		return new User(name,email,newUserId);
	}

	public User authenticate(String email, String clearTextPassword) {
		ListOfRows rows = database.select("select * from users where email = ?", email);
		if (rows.size() == 0)
			return null;
		String name = (String) rows.get(0).get("name");
		String password = (String) rows.get(0).get("psw");
		Integer id = (Integer) rows.get(0).get("id");

		try {
			if(Password.check(clearTextPassword,password))
				return new User(name,email,id);
			else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

    @Override
    public List<User> getAllUsers() {
        String sql = "select * from users order by id";
        ListOfRows rows = database.select(sql);
        List users = new LinkedList();
        for(Map<String,Object> row : rows){
            String name =  (String) row.get("name");
            String email = (String) row.get("email");
            int id = (Integer) row.get("id");
            users.add(new User(name,email,id));
        }
        return users;
    }

    @Override
    public void clear() {
        database.execute("truncate users cascade");
    }

}
