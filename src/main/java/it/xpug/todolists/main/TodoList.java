package it.xpug.todolists.main;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;

public class TodoList {
	private int id;
	private String name;
	private List<TodoItem> todoItems = new ArrayList<>();
	private User owner;

	public TodoList(String name, User owner) {
		this.name = name;
		this.owner = owner;
	}


	public TodoList(int todoListId, String name, User owner) {
		id = todoListId;
		this.name = name;
		this.owner = owner;
	}

	public TodoList(int id, String name, List<TodoItem> todoItems, User owner) {
		this.id = id;
		this.name = name;
		this.todoItems = todoItems;
		this.owner = owner;
	}

	public String getName() {
	    return name;
    }

	public void addItem(TodoItem todoItem) {
		todoItems.add(todoItem);
    }

	public JSONObject toJson() {
	    JSONObject result = new JSONObject()
	    	.put("name", getName())
	    	.put("items", emptyList());
	    for (TodoItem todoItem : todoItems) {
	        result.append("items", todoItem.toJson());
        }
	    return result;
    }

	public void checkItem(int todoItemId, boolean checked) {
		todoItems.get(todoItemId).setChecked(checked);
    }

	public int getId() {
	    return id;
    }

	public void setId(int id) {
		this.id = id;
    }

	public List<TodoItem> getItems() {
	    return new ArrayList<TodoItem>(todoItems);
    }

	public int getItemsLeft() {
		int tot= todoItems.size();
//		System.out.println(DEBUG: todoItems.size());
		for (TodoItem todoItem : todoItems) {
			if (todoItem.isChecked()){
				tot--;
			}
		}
		return tot;
	};

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	@Override
    public String toString(){
        return "TodoList \""+name+"\", id:"+getId()+", owner: "+owner+"\n";
    }
}
