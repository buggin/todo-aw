package it.xpug.todolists.main;

public class TodoListSession {

    private final User user;
    private final String id;

	public TodoListSession(String id, User user) {
		this.id = id;
        this.user = user;
	}

	public String getId() {
	    return id;
    }
    public User getUser() {return user;}
}
