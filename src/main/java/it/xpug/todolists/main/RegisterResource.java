package it.xpug.todolists.main;

import org.json.JSONObject;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class RegisterResource extends Resource {

    private SessionRepository sessions;
    private DatabaseUserRepository databaseUserRepository;

    public RegisterResource(HttpServletRequest request, HttpServletResponse response, SessionRepository sessions, UserRepository userRepository) {
        super(request, response);
        this.sessions = sessions;
        this.databaseUserRepository = (DatabaseUserRepository) userRepository;
    }

    @Override
    public void service() throws IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String username = request.getParameter("name");
        User newUser = new User(username, email);
        newUser.setId(databaseUserRepository.add(password,newUser));
        if(newUser.getId()!=-1){
            String newSessionId = sessions.newSessionId();
            sessions.add(new TodoListSession(newSessionId, newUser));
            response.addCookie(new Cookie(AuthenticationFilter.SESSION_COOKIE, newSessionId));
        }else{
            response.setStatus(403);
            render(new JSONObject().put("message", "Email already taken"));
        }
    }
}

