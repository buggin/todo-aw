package it.xpug.todolists.main;

import org.json.JSONObject;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginResource extends Resource {

	private SessionRepository sessions;
	private DatabaseUserRepository databaseUserRepository;

	public LoginResource(HttpServletRequest request, HttpServletResponse response, SessionRepository sessions, UserRepository userRepository) {
	    super(request, response);
		this.sessions = sessions;
		this.databaseUserRepository = (DatabaseUserRepository) userRepository;
    }

	@Override
	public void service() throws IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		User found = databaseUserRepository.authenticate(email, password);
		if (null!=found){
			String newSessionId = sessions.newSessionId();
			sessions.add(new TodoListSession(newSessionId, found));
			response.addCookie(new Cookie(AuthenticationFilter.SESSION_COOKIE, newSessionId));
		} else {
			response.setStatus(403);
			render(new JSONObject().put("message", "Email or password not valid"));
		}
	}
}
