package it.xpug.todolists.main;


import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.DatabaseConfiguration;
import it.xpug.toolkit.web.ReusableJettyApp;

import static java.lang.Integer.valueOf;


public class Main {
	private static final String LOCAL_DATABASE_URL = "postgres://todolists:secret@localhost:5432/todolists_development";

	public static void main(String[] args) {
		String port = System.getenv("PORT");
        String db_url = System.getenv("DATABASE_URL");
		if (port == null || port.isEmpty()) {
			port = "8080";
        }
        if (db_url== null|| db_url.isEmpty()){
            db_url = LOCAL_DATABASE_URL;
        }

		InMemorySessionRepository sessionRepository = new InMemorySessionRepository();
        System.out.println("db URL:\n"+db_url);
		DatabaseConfiguration configuration = new DatabaseConfiguration(db_url);
		Database database = new Database(configuration);
		TodoListRepository todoLists = new DatabaseTodoListRepository(database);
		DatabaseUserRepository users = new DatabaseUserRepository(database);

		ReusableJettyApp app = new ReusableJettyApp(new TodoListsServlet(todoLists, sessionRepository, users));

		app.start(valueOf(port), "src/main/webapp");
	}
}
