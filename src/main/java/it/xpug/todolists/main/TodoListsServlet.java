package it.xpug.todolists.main;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TodoListsServlet extends HttpServlet {

	private TodoListRepository todoLists;
	private AuthenticationFilter authenticationFilter;
	private SessionRepository sessions;
	private UserRepository userRepository;

	public TodoListsServlet(TodoListRepository todoLists, SessionRepository sessions, UserRepository userRepository) {
		this.todoLists = todoLists;
		this.sessions = sessions;
		this.authenticationFilter = new AuthenticationFilter(sessions);
		this.userRepository = userRepository;
    }

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Serving: " + request.getMethod() + " " + request.getRequestURI());
		//delay();

		Resource resource = getResource(request, response);
		resource.service();
	}

	protected void delay() {
	    try {
	        Thread.sleep(1000);
        } catch (InterruptedException e) {
	        throw new RuntimeException(e);
        }
    }

	private Resource getResource(HttpServletRequest request, HttpServletResponse response) {
		if (request.getRequestURI().matches("/login")) {
			return new LoginResource(request, response, sessions, userRepository);
		}
		if (request.getRequestURI().matches("/register")) {
			return new RegisterResource(request, response, sessions, userRepository);
		}

		TodoListSession session = authenticationFilter.getSession(request.getCookies());
		if (null == session) {
			return new NotAuthorized(response);
		}
		if (request.getRequestURI().matches("/todolists/\\d+/users(/\\d+)?")) {
			return new TodoItemsResource(request,response, todoLists,session);
		}
		if (request.getRequestURI().matches("/todolists/\\d+/permissions")) {
			return new TodoItemsResource(request,response, todoLists,session);
		}
		if (request.getRequestURI().matches("/todolists/\\d+/items(/\\d+)?")) {
			return new TodoItemsResource(request, response, todoLists, session);
		}
		if (request.getRequestURI().matches("/todolists(/\\d+)?")) {
			return new TodoListsResource(request, response, todoLists, session);
		}
		if ("/".equals(request.getRequestURI())) {
			return new HelloWorldResource(response);
		}
	    return new Notfound(response);
    }

}
