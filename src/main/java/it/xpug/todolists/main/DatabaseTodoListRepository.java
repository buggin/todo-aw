package it.xpug.todolists.main;

import it.xpug.toolkit.db.*;

import java.util.*;

public class DatabaseTodoListRepository implements TodoListRepository{

	private Database database;

	public DatabaseTodoListRepository(Database database) {
		this.database = database;
	}

	@Override
	public TodoList get(int todoListId, User user) {
		ListOfRows rows = database.select("select * from todo_lists where id = ?", todoListId);
		if (rows.size() == 0){
            System.err.println("Todolist "+todoListId+" Not found!");
            return null;
        }
		String name = (String) rows.get(0).get("name");
        String owner = (String) rows.get(0).get("owner");
        User ownUser = getUserInfo(owner);
		TodoList todoList = new TodoList(todoListId, name, ownUser);
        Permission permission = getPermission(todoList,user);
        if(permission.canRead()) {
            String sql = "select * from todo_items where todo_list_id = ?";
            for (Map<String, Object> row : database.select(sql, todoListId)) {
                String text = (String) row.get("text");
                int todoItemId = (int) row.get("id");
                boolean isChecked = (boolean) row.get("checked");
                todoList.addItem(new TodoItem(todoItemId, text, isChecked));
            }
            return todoList;
        }
        else {
            System.err.println("User has no permission to read!");
            return null;
        }
	}

    private TodoList getTodolist(int todoListId){

        ListOfRows rows = database.select("select * from todo_lists where id = ?", todoListId);
        if (rows.size() == 0){
            System.err.println("Todolist "+todoListId+" Not found!");
            return null;
        }
        String name = (String) rows.get(0).get("name");
        String owner = (String) rows.get(0).get("owner");
        User ownUser = getUserInfo(owner);
        TodoList todoList = new TodoList(todoListId, name, ownUser);
        return todoList;
    }
	@Override
	public int add(TodoList todoList) {
		String sql = "insert into todo_lists (name,owner) values (?,?) returning id";
		ListOfRows rows = database.select(sql, todoList.getName(),todoList.getOwner().getEmail());
		int id = (int) rows.get(0).get("id");
		todoList.setId(id);
        sql = "insert into permission (user_id,todo_list_id,permission_code) values" +
                "(?,?,?)";
        database.execute(sql,todoList.getOwner().getId(),todoList.getId(),111);
		return id;
	}

	@Override
	public List<TodoList> getAll(User user) {
		List<TodoList> result = new ArrayList<TodoList>();
		ListOfRows rows = database.select("select * from todo_lists order by id");
		for (Map<String, Object> row : rows) {
			String name = (String) row.get("name");
			int id = (int) row.get("id");
            User owner = getUserInfo((String)row.get("owner"));
            TodoList todoList = new TodoList(id, name, owner);
            Permission permission = getPermission(todoList,user);
            if(permission.canRead()) {
//                System.out.println("Adding "+todoList);
                result.add(todoList);
            }
        }
		return result;
	}
    @Override
    public User getUserInfo(String email){

        String sql = "select * from users where email = ?";
        ListOfRows rows = database.select(sql, email);
        String name = (String) rows.get(0).get("name");
        sql = "select id from users where email = ?";
        int id = (Integer) database.selectOneValue(sql,email);
        return new User(name,email, id);
    }
	@Override
	public void clear() {
		database.execute("truncate todo_lists cascade");
	}

	@Override
	public long size() {
		return (long) database.selectOneValue("select count(*) from todo_lists");
	}

	@Override
    public void update(TodoList todoList, User user) {
        Permission permission = getPermission(todoList,user);
        if (permission.canRead())
		for (TodoItem todoItem : todoList.getItems()) {
			if (todoItem.getId() == null&&permission.canAdd()) {
				String sql = "insert into todo_items (todo_list_id, text, checked)"
						+ "values (?, ?, ?)";
				database.execute(sql, todoList.getId(), todoItem.getText(), todoItem.isChecked());
			}
			else {
                if(permission.canCheck()) {
                    String sql = "update todo_items set checked = ? where todo_list_id = ? and id = ?";
                    database.execute(sql, todoItem.isChecked(), todoList.getId(), todoItem.getId());
                }
			}
        }
    }
    public PermissionCode getPermission(int todoId, User user){
        int intCode = (Integer) database.selectOneValue("select permission_code from permission " +
                "where user_id = ? and todo_list_id = ?", user.getId(), todoId);
        return new PermissionCode(intCode);
    }
    public Permission getPermission(TodoList todoList, User user){

        int intCode = (Integer) database.selectOneValue("select permission_code from permission " +
                "where user_id = ? and todo_list_id = ?", user.getId(), todoList.getId());
        System.out.println("DEBUG: code "+intCode);
        Permission p = new Permission(todoList,user,new PermissionCode(intCode));
        System.out.println("DEBUG: from db permission got\t" + p);

        return p;
    }

    @Override
    public void setName(TodoList todoList, String name, User user) {
        if (todoList.getOwner().getId()==user.getId()){
            String sql = "update todo_lists set name = ? where owner = ? and id = ? and name = ?";
            database.execute(sql, name, user.getEmail(), todoList.getId(), todoList.getName());
        }
    }

    @Override
	public int getNItemsLeft(TodoList todoList, User user) {
		todoList = get(todoList.getId(),user);
		return todoList.getItemsLeft();
	}

    @Override
    public void grantAccess(TodoList todoList, User owner, User anotherUser, Permission permission) {
        if(todoList.getOwner().getEmail().equals(owner.getEmail())){
            writePermission(todoList, anotherUser, permission.getCode());
        }
    }
    private void writePermission(TodoList todoList, User user, PermissionCode code) {
        System.out.println("updating code with " + code.getCode());
        database.execute("DELETE FROM permission where user_id = ? and todo_list_id = ?", user.getId(), todoList.getId());
        String sql = "insert into permission (user_id,todo_list_id,permission_code) values (?,?,?)";
        database.execute(sql, user.getId(), todoList.getId(), code.getCode());
        System.out.println("DEBUG WRITING PERMISSION: read from db again");
        System.out.println(getPermission(todoList,user));
    }

    @Override
    public LinkedList<Sharing> getAllUsers(TodoList todoList) {
        LinkedList<Sharing> users = new LinkedList<>();
        String sql = "select user_id,permission_code from permission where todo_list_id = ?";
        ListOfRows rows = database.select(sql,todoList.getId());
        for(Map<String, Object> row: rows) {
            sql = "select * from users where id = ?";
            int userId = (Integer) row.get("user_id");
            PermissionCode code = new PermissionCode((Integer)row.get("permission_code"));
            ListOfRows rows1 = database.select(sql, userId);
            for (Map<String, Object> row1 : rows1) {
                String name = (String) row1.get("name");
                String email = (String) row1.get("email");
                User newuser = new User(name, email, userId);
                Permission permission = new Permission(todoList,newuser,code);
                users.add(new Sharing(permission, newuser, todoList));
            }
        }
        return users;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        String sql = "SELECT count(*) FROM users;";
        long size = (Long) database.selectOneValue(sql);
        sql = "select * from users;";
        ListOfRows rows = database.select(sql);
        for(Map<String, Object> row: rows) {
            String name = (String) row.get("name");
            String email = (String) row.get("email");
            int userId = (Integer) row.get("id");
            User newuser = new User(name, email, userId);
            users.add(newuser);
        }
        return users;
    }


}
