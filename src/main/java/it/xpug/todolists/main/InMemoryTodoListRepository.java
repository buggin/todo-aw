package it.xpug.todolists.main;

import it.xpug.toolkit.db.Permission;
import it.xpug.toolkit.db.PermissionCode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.synchronizedList;

public class InMemoryTodoListRepository implements TodoListRepository {
    private List<TodoList> todoLists = synchronizedList(new ArrayList<TodoList>());
    private List<Permission> permissions = new ArrayList<>();
//TODO grosso come una casa

    @Override
    public int add(TodoList todoList) {

        synchronized (todoLists) {
            todoLists.add(todoList);
            int id= todoLists.size()-1;
            todoList.setId(id);
            permissions.add(id,new Permission(todoList,todoList.getOwner(),new PermissionCode(7)));
            return todoList.getId();
        }
    }

    @Override
    public List<TodoList> getAll(User user) {
        return new ArrayList<TodoList>(todoLists);
    }

    @Override
    public TodoList get(int todoListId, User user) {
        for(TodoList t: todoLists){
            if(t.getOwner().equals(user)){
                Permission p = getPermission(t,user);
                if(p.canRead()){
                    return todoLists.get(todoListId);
                }else {
                    System.err.println("User "+user+"cannot read!"+p);
                    return null;
                }
            }
        }
        System.err.println("Disagio");
        return null;
    }

    @Override
    public void update(TodoList todoList, User user) {

    }

    @Override
    public int getNItemsLeft(TodoList todoList, User user) {
        return 0;
    }

    @Override
    public void grantAccess(TodoList todoList, User owner, User anotherUser, Permission permission) {

    }



    @Override
    public LinkedList<Sharing> getAllUsers(TodoList todoList) {
        return null;
    }

    @Override
    public List<User> getAllUsers() {
        return null;
    }

    @Override
    public Permission getPermission(TodoList todolist, User user) {
        for(Permission p : permissions) {
            if (p.getTodoList().getId() == todolist.getId()) {
                System.out.println("DEBUG:Permission Found" + p);
                return p;
            }
        }
        return new Permission(todolist,user,new PermissionCode(0));
    }

    @Override
    public void setName(TodoList todoList, String name, User user) {

    }

    @Override
    public User getUserInfo(String email) {
        return null;
    }

    @Override
    public void clear() {
        todoLists.clear();
    }

    @Override
    public long size() {
        return todoLists.size();
    }


}
