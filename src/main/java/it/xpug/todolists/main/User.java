package it.xpug.todolists.main;

public class User {

	private String name;
	private String email;
	private int id;

	public User(String name, String email) {
		this.name = name;
		this.email = email;
    }

	public User(String name, String email, int id) {
		this.name = name;
		this.email = email;
		this.id = id;
	}

	public String getEmail() {
	    return email;
    }

	public String getName() {
	    return name;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    @Override
    public String toString(){
        return "UserName:\""+getName()+"\" <"+getEmail()+"> User-id:"+getId()+"\n";
    }
}
