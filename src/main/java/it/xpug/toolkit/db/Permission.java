package it.xpug.toolkit.db;

import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;

public class Permission {

    private final TodoList todoList;
    private final User user;
    private final PermissionCode code;

    public Permission(TodoList todoList, User user, PermissionCode code) {

        this.todoList = todoList;
        this.user = user;
        this.code = code;
    }

    public PermissionCode getCode() {
        return code;
    }

    public int getCodeAsInt() {return code.getCode();};

    public User getUser() {
        return user;
    }

    public TodoList getTodoList() {
        return todoList;
    }

    public boolean canRead(){ return code.canRead();}
    public boolean canAdd(){
        return code.canAdd();
    }
    public boolean canCheck(){
        return code.canCheck();
    }

    public void setCheck(){code.setCheck();}
    public void setAdd(){code.setAdd();}
    public void setRead(){code.setRead();}

    @Override
    public String toString(){
        return user.getName()+" can "+code.toString()+" for list#"+todoList.getId();
    }
}
