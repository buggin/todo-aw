package it.xpug.toolkit.db;

public class PermissionCode {

    private boolean r = false;
    private boolean c = false;
    private boolean a = false;
    private int code;

    public PermissionCode(int code) {
        this.code = code;
        switch (code){
            case 1:
                a = true;
                break;
            case 10:
                c = true;
                break;
            case 11:
                c = a = true;
                break;
            case 100:
                r = true;
                break;
            case 101:
                r = a = true;
                break;
            case 110:
                r = c = true;
                break;
            case 111:
                r = c = a = true;
                break;
        }
    }

    boolean canAdd(){
        return a;
    }
    boolean canCheck(){
        return c;
    }
    boolean canRead(){
        return r;
    }
    void setRead() {
        r = !r;
    }
    void setCheck() {
        c = !c;
    }
    void setAdd() {
        a = !a;
    }
    public int getCode(){
        if(r&c&a) return 111;
        if(r&c&!a) return 110;
        if(r&!c&a)return 101;
        if(r&!c&!a)return 100;
        if(!r&c&a)return 11;
        if(!r&c&!a)return 10;
        if(!r&!c&a)return 1;
        if(!r&!c&!a)return 0;
        return -1;
    };
    @Override
    public String toString(){
        String p = "";
        if(canRead())
            p+=" Read";
        else p+=" NoRead";
        if(canAdd())
            p+=" Add";
        else p+=" NoAdd";
        if(canCheck())
            p+=" Check";
        else p+=" NoCheck";
        return p;
    }
}
