package it.xpug.toolkit.db;

import it.xpug.todolists.main.TodoList;
import it.xpug.todolists.main.User;

public interface PermissionRepository {
    public Permission getPermission(TodoList todolist, User user);
}
