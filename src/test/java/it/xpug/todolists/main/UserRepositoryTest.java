package it.xpug.todolists.main;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserRepositoryTest {

	@Test
    public void addUser() throws Exception {
	    InMemoryUserRepository repository = new InMemoryUserRepository();
	    User user = new User("Matteo", "matteo@matteo.com");
		repository.add(user, "password");

	    assertEquals("wrong password", null, repository.authenticate("matteo@matteo.com", "wrong password"));
	    assertEquals("wrong email", null, repository.authenticate("wrong email", "password"));
	    assertEquals(user, repository.authenticate("matteo@matteo.com", "password"));
    }

	@Test
	public void getAllUsers() throws Exception {
		InMemoryUserRepository repository = new InMemoryUserRepository();
		User user = new User("Matteo", "matteo@matteo.com");
		repository.add(user, "password");
		user = new User ("Matteo","matteo@2.com");
		repository.add(user,"password");

		assertEquals("Matteo <matteo@matteo.com>\nMatteo <matteo@2.com>\n",repository.getAllUsers());

	}
}
