package it.xpug.todolists.main;

import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.DatabaseConfiguration;
import it.xpug.toolkit.db.DatabaseTest;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DatabaseTodoListRepositoryTest {
	private DatabaseConfiguration configuration = new DatabaseConfiguration(DatabaseTest.TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	private DatabaseTodoListRepository todolists = new DatabaseTodoListRepository(database );
    private InMemorySessionRepository sessions = new InMemorySessionRepository();
    private UserRepository userRepository = new DatabaseUserRepository(database);
    private User user;
	@Before
	public void clearRepository() {
		todolists.clear();
        userRepository.clear();
	}

    @Before
    public void setUp() throws Exception {
        user = new User("Matteo", "@prova.com");
        userRepository.add("password",user);
        sessions.add(new TodoListSession("1234567890", user));
    }

    @Before

    @Test
    public void initiallyEmpty() throws Exception {
		assertEquals("repo initially empty", 0, todolists.size());
    }

	@Test
    public void insertOneElement() throws Exception {

	    TodoList todoList = new TodoList("prova",user);
		int newId = todolists.add(todoList);

		assertEquals("repo now contains 1", 1, todolists.size());
		assertEquals(newId, todoList.getId());
    }

	@Test
    public void getOneElement() throws Exception {

	    TodoList todoList = new TodoList("prova",user);
		int id = todolists.add(todoList);

		TodoList todoListFromRepo = todolists.get(id,user);

		assertEquals("prova", todoListFromRepo.getName());
		assertEquals(id, todoListFromRepo.getId());
    }

	@Test
    public void notFound() throws Exception {

	    assertNull("not found", todolists.get(9786987, user));
    }

	@Test
	public void allLists() throws Exception {

	    int id0 = todolists.add(new TodoList("zero",user));
	    int id1 = todolists.add(new TodoList("one",user));

	    List<TodoList> allLists = todolists.getAll(user);

	    assertEquals(2, allLists.size());
	    assertEquals("zero", allLists.get(0).getName());
	    assertEquals(id0, allLists.get(0).getId());
	    assertEquals("one", allLists.get(1).getName());
	    assertEquals(id1, allLists.get(1).getId());
	}

	@Test
    public void updateListWithNewTodoItem() throws Exception {

	    TodoList todoList = new TodoList("zero",user);
		int todoListId = todolists.add(todoList);
		todoList.setId(todoListId);

		TodoItem todoItem = new TodoItem("pippo");
		todoItem.setChecked(true);
		todoList.addItem(todoItem);

		todolists.update(todoList,user);

		TodoList foundList = todolists.get(todoListId,user);
		List<TodoItem> todoItems = foundList.getItems();
		assertEquals(1, todoItems.size());
		assertEquals("pippo", todoItems.get(0).getText());
		assertEquals(true, todoItems.get(0).isChecked());
    }
}
