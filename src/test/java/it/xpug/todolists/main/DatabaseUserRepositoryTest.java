package it.xpug.todolists.main;

import it.xpug.toolkit.db.Database;
import it.xpug.toolkit.db.DatabaseConfiguration;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DatabaseUserRepositoryTest {

	public static final String TEST_DATABASE_URL = "postgres://todolists:secret@localhost:5432/todolists_test";
	private DatabaseConfiguration configuration = new DatabaseConfiguration(TEST_DATABASE_URL);
	private Database database = new Database(configuration);
	DatabaseUserRepository repository = new DatabaseUserRepository(database);

	@Test
    public void saveAUser() throws Exception {
		int newUserId = repository.add("password", new User("name", "email2"));

		User found = repository.get(newUserId);
		assertEquals("name", found.getName());
		assertEquals("email2", found.getEmail());
    }

	@Test
    public void authenticateAUser() throws Exception {
		repository.add("password", new User("name", "email"));

		User found = repository.authenticate("email", "password");
		assertEquals("name", found.getName());
		assertEquals("email", found.getEmail());

		assertNull("wrong password", repository.authenticate("email", "BAD PASSWORD"));
		assertNull("wrong email", repository.authenticate("WRONG EMAIL", "password"));
    }

}
