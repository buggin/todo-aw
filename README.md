# Demo site

[https://aw-buggin.herokuapp.com/
](https://aw-buggin.herokuapp.com/)

#Feature:
- Bootstrap framework
- Adding lists of todos
- Adding todos
- Sharing list with other users, with different degrees of permission (Read Write Check, Read only, Read and Check) [iirc]
- Authentication Server
- Java Backend (Jetty)
- Postgresql Database to serve todo items/users/lists
- AJAX / Rest API
- JQuery

# Build from Source

Install Maven.

Then, run

    mvn eclipse:clean eclipse:eclipse -DdownloadSources -DdownloadJavadocs

Then, use Import... > Existing Project in Eclipse.

## Run the project

In Eclipse, right-click on the it.xpug.todolists.main.Main class and select Debug As... > Java Application.

Then observe the application at http://localhost:8080/

It should work out of the box.
